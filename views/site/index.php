<?php

use yii\grid\GridView;

$this->title= $titulo;
?>

<h1><?= $titulo ?></h1>
<br>
<br>
<?php

echo GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" =>[
        [
            "attribute"=> "title",
            "label" => "Titulo"
        ],
        [
            "attribute"=>"link",
            "label" => "Enlace",
            "content" => function($model){
                return yii\helpers\Html::a(
                        "Ver noticia",
                        $model->link,
                        ["class" => "btn btn-primary"]
                );
            }
        ],
        [
            "attribute"=>"description",
            "label" => "Descripcion",
            "content" => function($model){
                $contenido= str_replace(
                        "src",  // busco la palabra src
                        'class="img-thumbnail" src',  // coloco delante de src
                        $model->description // donde busco
                        );
                return $contenido;
            }
        ]
    ]
]);