<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // direccion del canal rss
        $url="https://www.eldiariomontanes.es/rss/2.0/?section=ultima-hora";
        
        // leer una pagina web
        $contenido=file_get_contents($url);
        
        // funcion de php lea xml y lo convierta a array
        $ObjetoTiponoticias=simplexml_load_string($contenido)->channel->item;
        
        // crear un array con todas las noticias
        foreach ($ObjetoTiponoticias as $noticia){
            $noticias[]=$noticia;
        }
        
        // noticias es una array con todas noticias
        // mostreis todas las noticias en un GRIDVIEW
        
        // creo un dataProvider con arrayDataProvider
        $dataProvider=new \yii\data\ArrayDataProvider([
            "allModels" => $noticias,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        
        return $this->render("index",[
            "dataProvider" => $dataProvider,
            "titulo" => "Noticias ultima hora"
        ]);        
        
    }
    
    public function actionCine()
    {
        // direccion del canal rss
        $url="https://www.eldiariomontanes.es/rss/2.0/?section=butaca/cine";
        
        // leer una pagina web
        $contenido=file_get_contents($url);
        
        // funcion de php lea xml y lo convierta a array
        $ObjetoTiponoticias=simplexml_load_string($contenido)->channel->item;
        
        // crear un array con todas las noticias
        foreach ($ObjetoTiponoticias as $noticia){
            $noticias[]=$noticia;
        }
        
        // noticias es una array con todas noticias
        // mostreis todas las noticias en un GRIDVIEW
        
        // creo un dataProvider con arrayDataProvider
        $dataProvider=new \yii\data\ArrayDataProvider([
            "allModels" => $noticias,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        
        return $this->render("index",[
            "dataProvider" => $dataProvider,
            "titulo" => "Noticias cine"
        ]);        
        
    }
    
    public function actionRecetas()
    {
        // direccion del canal rss
        $url="https://www.eldiariomontanes.es/rss/2.0/?section=cantabria-mesa/recetas";
        
        // leer una pagina web
        $contenido=file_get_contents($url);
        
        // funcion de php lea xml y lo convierta a array
        $ObjetoTiponoticias=simplexml_load_string($contenido)->channel->item;
        
        // crear un array con todas las noticias
        foreach ($ObjetoTiponoticias as $noticia){
            $noticias[]=$noticia;
        }
        
        // noticias es una array con todas noticias
        // mostreis todas las noticias en un GRIDVIEW
        
        // creo un dataProvider con arrayDataProvider
        $dataProvider=new \yii\data\ArrayDataProvider([
            "allModels" => $noticias,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        
        return $this->render("index",[
            "dataProvider" => $dataProvider,
            "titulo" => "Recetas"
        ]);        
        
    }

    
}
